import { Button, Typography } from '@material-ui/core';
import React, { FC, useState } from 'react';
import { useDispatch } from 'react-redux';
import { show as showLoader, hide as hideLoader } from '../../store/slices/loader.slice';
import { toast } from 'react-toastify';

export const Home: FC = () => {
    const dispatch = useDispatch();
    const [visible, setVisible] = useState(false);

    const handleClick = () => {
        dispatch(visible ? hideLoader() : showLoader());
        setVisible(!visible);
    }

    return (
        <div>
            <Typography variant="h4">Dashboard</Typography>
            <Button color="primary" onClick={() => handleClick()}>Toogle loader</Button>
            <Button color="primary" onClick={() => toast.success("Hello!")}>Notify</Button>
        </div >
    )
}