import React, { FC } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { NotFound } from './not-found/NotFound';
import { UnderDevelopment } from './under-development/UnderDevelopment';
import { Home } from './home/Home';

export const RouterView: FC = () => {
    return (
        <Switch>
            <Route path="/404" component={NotFound} />
            <Route path="/under-development" component={UnderDevelopment} />
            <Route path="/institutes">
                <Redirect to="/under-development" />
            </Route>
            <Route path="/departments">
                <Redirect to="/under-development" />
            </Route>
            <Route path="/groups">
                <Redirect to="/under-development" />
            </Route>
            <Route path="/students">
                <Redirect to="/under-development" />
            </Route>
            <Route path="/subjects">
                <Redirect to="/under-development" />
            </Route>
            <Route path="/lessons">
                <Redirect to="/under-development" />
            </Route>
            <Route path="/attendance">
                <Redirect to="/under-development" />
            </Route>
            <Route path="/grades">
                <Redirect to="/under-development" />
            </Route>
            <Route path="/reports">
                <Redirect to="/under-development" />
            </Route>
            <Route path="/import">
                <Redirect to="/under-development" />
            </Route>
            <Route path="/settings">
                <Redirect to="/under-development" />
            </Route>
            <Route path="/about">
                <Redirect to="/under-development" />
            </Route>
            <Route path="/" exact component={Home} />
            <Route path="/login" exact>
                <Redirect to="/under-development" />
            </Route>
            <Redirect to="/404" />
        </Switch>)
}