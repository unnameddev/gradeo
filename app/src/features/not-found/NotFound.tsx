import React, { FC } from "react";
import NotFoundImage from '../../assets/images/404.svg'
import { Grid, Typography, Box, Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import { ArrowBack } from "@material-ui/icons";

export const NotFound: FC = () => {
    return (
        <Grid container justify="center" spacing={1}>
            <Grid item>
                <img src={NotFoundImage} width="256" height="256" alt="Page not found" />
            </Grid>
            <Grid item xs={12} container justify="center">
                <Typography variant="h5">
                    Эта страница потерялась
                </Typography>
            </Grid>
            <Grid item xs={12} container justify="center">
                <Box color="text.secondary">
                    <Typography variant="subtitle1">
                        Но вы всегда можете поставить ей Н-ку
                    </Typography>
                </Box>
            </Grid>
            <Grid item xs={12} container justify="center">
                <Button 
                    color="primary" 
                    component={Link} 
                    to="/"
                    startIcon = {<ArrowBack />}
                >
                    Вернуться на главную
                </Button>
            </Grid>
        </Grid>
    )
}