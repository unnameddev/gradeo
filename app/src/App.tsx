import React, { FC } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { Layout } from './layout/Layout';
import { ThemeProvider } from '@material-ui/core';
import { Provider as StateProvider } from 'react-redux';
import useThemes from './theme';
import { store } from './store/store';

export const App: FC = () => {
  return (
    <StateProvider store={store}>
      <ThemeProvider theme={useThemes}>
        <Router>
          <Layout />
          <ToastContainer position="bottom-right" />
        </Router>
      </ThemeProvider>
    </StateProvider>
  )
}
