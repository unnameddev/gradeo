import React, { FC, useState } from 'react';
import { AppNavBar } from './navbar/AppNavBar';
import { AppDrawer } from './drawer/AppDrawer';
import { makeStyles, CssBaseline, List, Divider } from '@material-ui/core';
import { DrawerLink } from './drawer/DrawerLink';
import { DrawerSection } from './drawer/DrawerSection';
import HomeIcon from '@material-ui/icons/Home';
import InfoIcon from '@material-ui/icons/Info';
import SettingsIcon from '@material-ui/icons/Settings';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import DescriptionOutlinedIcon from '@material-ui/icons/DescriptionOutlined';
import SpellcheckOutlinedIcon from '@material-ui/icons/SpellcheckOutlined';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import WidgetsOutlinedIcon from '@material-ui/icons/WidgetsOutlined';
import { RouterView } from '../features/RouterView';
import { AppLoader } from './loader/AppLoader';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    closeMenuButton: {
        marginRight: 'auto',
        marginLeft: 0,
    },
}));

export const Layout: FC = () => {
    const classes = useStyles();

    const [mobileOpen, setMobileOpen] = useState(false);

    function handleDrawerToggle() {
        setMobileOpen(!mobileOpen)
    }

    return (
        <>
            <AppLoader />
            <div className={classes.root}>
                <CssBaseline />
                <AppNavBar onDrawerToggle={handleDrawerToggle} />
                <AppDrawer mobileOpen={mobileOpen} onDrawerToggle={handleDrawerToggle}>
                    <List>
                        <DrawerLink title="Главная" to="/" icon={<HomeIcon />} />
                        <DrawerSection title="Данные" icon={<WidgetsOutlinedIcon />}>
                            <DrawerLink title="Институты" to="/institutes" />
                            <DrawerLink title="Кафедры" to="/departments" />
                            <DrawerLink title="Группы" to="/groups" />
                            <DrawerLink title="Студенты" to="/students" />
                            <DrawerLink title="Предметы" to="/subjects" />
                            <DrawerLink title="Занятия" to="/lessons" />
                        </DrawerSection>
                        <DrawerLink title="Посещаемость" to="/attendance" icon={<AssignmentTurnedInIcon />} />
                        <DrawerLink title="Оценивание" to="/grades" icon={<SpellcheckOutlinedIcon />} />
                        <DrawerLink title="Отчеты" to="/reports" icon={<DescriptionOutlinedIcon />} />
                    </List>
                    <Divider />
                <List>
                    <DrawerLink title="Импорт/Экспорт" to="/import" icon={<ImportExportIcon />} />
                    <DrawerLink title="Настройки" to="/settings" icon={<SettingsIcon />} />
                    <DrawerLink title="О Gradeo" to="/about" icon={<InfoIcon />} />
                </List>
                </AppDrawer>
                <main className={classes.content}>
                    <div className={classes.toolbar} />
                    <RouterView />
                </main>
            </div>
        </>
    )
}