import React, { FC } from "react";
import { AppBar, Toolbar, IconButton, Chip, Theme, makeStyles, createStyles } from "@material-ui/core";
import Logo from '../../assets/images/logo.svg'
import MenuIcon from '@material-ui/icons/Menu';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        logo: {
            display: 'flex',
            marginLeft: '16px',
            marginRight: '16px',
            alignContent: 'center',
        },
        appBar: {
            borderBottom: `1px solid ${theme.palette.grey[300]}`,
            zIndex: theme.zIndex.drawer + 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
            [theme.breakpoints.up('sm')]: {
                display: 'none',
            },
        },
    }),
);

interface AppNavbarProps {
    onDrawerToggle: () => void,
}

export const AppNavBar: FC<AppNavbarProps> = ({ onDrawerToggle }) => {
    const classes = useStyles();

    return (
        <AppBar
            position="fixed"
            elevation={0}
            className={classes.appBar}
            color="secondary"
        >
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="Open drawer"
                    edge="start"
                    onClick={onDrawerToggle}
                    className={classes.menuButton}
                >
                    <MenuIcon />
                </IconButton>
                <div className={classes.logo}>
                    <img src={Logo} width="100" height="24" alt="Gradeo" />
                    <Chip size="small" style={{ marginLeft: 8 }} label="alpha" />
                </div>
            </Toolbar>
        </AppBar>
    );
}