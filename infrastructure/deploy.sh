#!/usr/bin/env bash

scp -v -i gradeo docker-compose.yml gradeo@$SERVER_URL:gradeo/docker-compose.yml
ssh -v -i gradeo -t gradeo@$SERVER_URL "cd gradeo && docker-compose stop && docker-compose pull && docker-compose up -d"
