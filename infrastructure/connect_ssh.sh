#!/usr/bin/env bash

# скачиваем SSH клиент
apt update && apt install openssh-client -y -qq
eval $(ssh-agent -s)
mkdir -p ~/.ssh
# копируем приватный ключ из переменной
echo "$SSH_PRIVATE_KEY" > gradeo
chmod 600 gradeo
# копируем ключ сервера из переменной
echo "$SSH_SERVER_HOSTKEY" > ~/.ssh/known_hosts
chmod 700 ~/.ssh
