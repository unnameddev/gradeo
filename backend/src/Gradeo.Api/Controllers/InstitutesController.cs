﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gradeo.Core.Dtos.Institutes;
using Gradeo.Core.Services;
using Gradeo.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Gradeo.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class InstitutesController : ControllerBase
    {
        private readonly InstitutesService _service;
        private readonly ILogger<InstitutesController> _logger;

        public InstitutesController(InstitutesService service, ILogger<InstitutesController> logger)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        public async Task<List<InstituteListItemDto>> Get()
        {
            return await _service.GetAll();
        }
    }
}
