﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gradeo.Core.Dtos.Departments;
using Gradeo.Core.Services;
using Gradeo.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Gradeo.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DepartmentsController : ControllerBase
    {
        private readonly DepartmentsService _service;
        private readonly ILogger<DepartmentsController> _logger;

        public DepartmentsController(DepartmentsService service, ILogger<DepartmentsController> logger)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        public async Task<List<DepartmentListItemDto>> Get()
        {
            return await _service.GetAll();
        }
    }
}
