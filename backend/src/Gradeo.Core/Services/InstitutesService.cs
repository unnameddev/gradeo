﻿using Gradeo.Core.Abstractions;
using Gradeo.Core.Dtos.Institutes;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gradeo.Core.Services
{
    public class InstitutesService
    {
        private readonly IInstitutesRepository _repository;

        public InstitutesService(IInstitutesRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Возвращает список всех институтов
        /// </summary>
        /// <returns>Список всех институтов</returns>
        public async Task<List<InstituteListItemDto>> GetAll()
        {
            var institutes = await _repository.Get();
            return institutes
                .Select(institute => new InstituteListItemDto
                {
                    Id = institute.Id,
                    Name = institute.Name,
                    ShortName = institute.ShortName
                })
                .ToList();
        }
    }
}
