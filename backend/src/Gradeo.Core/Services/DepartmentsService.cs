﻿using Gradeo.Core.Abstractions;
using Gradeo.Core.Dtos.Departments;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gradeo.Core.Services
{
    public class DepartmentsService
    {
        private readonly IDepartmentsRepository _repository;

        public DepartmentsService(IDepartmentsRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Возвращает список всех кафедр
        /// </summary>
        /// <returns>Список всех кафедр</returns>
        public async Task<List<DepartmentListItemDto>> GetAll()
        {
            var departments = await _repository.Get();
            return departments
                .Select(department => new DepartmentListItemDto
                {
                    Id = department.Id,
                    Name = department.Name,
                    ShortName = department.ShortName
                })
                .ToList();
        }
    }
}
