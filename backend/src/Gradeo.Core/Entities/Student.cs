﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gradeo.Core.Entities
{
    public class Student : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public int GroupId { get; set; }
    }
}
