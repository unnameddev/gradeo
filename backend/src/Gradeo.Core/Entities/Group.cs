﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gradeo.Core.Entities
{
    public class Group : BaseEntity
    {
        public string Name { get; set; }
        public DateTime FormDate { get; set; }
        public int DepartmentId { get; set; }
    }
}
