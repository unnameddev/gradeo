﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gradeo.Core.Entities
{
    public class Department : BaseEntity
    {
        public string Name { get; set; }
        public string ShortName { get; set; }

        public int InstituteId { get; set; }
        public Institute Institute { get;set; }
    }
}
