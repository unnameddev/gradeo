﻿using Gradeo.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gradeo.Core.Abstractions
{
    // Прячем работу с базой за абстракцией для будущих тестов бизнес-логики
    public interface IInstitutesRepository
    {
        Task<IEnumerable<Institute>> Get();
    }
}
