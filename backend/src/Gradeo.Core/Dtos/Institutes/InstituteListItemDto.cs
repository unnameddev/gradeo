﻿using System.Collections.Generic;
using Gradeo.Core.Entities;

namespace Gradeo.Core.Dtos.Institutes
{
    public class InstituteListItemDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }
}
