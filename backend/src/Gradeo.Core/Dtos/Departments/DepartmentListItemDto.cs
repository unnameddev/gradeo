﻿using Gradeo.Core.Entities;

namespace Gradeo.Core.Dtos.Departments
{
    public class DepartmentListItemDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int InstituteId { get; set; }
        public string InstituteName { get; set; }

    }
}
