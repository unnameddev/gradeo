﻿using Gradeo.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Gradeo.Infrastructure
{
    public class GradeoDbContext : DbContext
    {
        public DbSet<Institute> Institutes { get; set; }
        public DbSet<Department> Departments { get; set; }

        public GradeoDbContext(DbContextOptions<GradeoDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Department>()
                .HasOne(p => p.Institute)
                .WithMany(b => b.Departments);
        }

    }
}
