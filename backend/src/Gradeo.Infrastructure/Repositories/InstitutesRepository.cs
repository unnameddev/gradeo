﻿using Gradeo.Core.Abstractions;
using Gradeo.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gradeo.Infrastructure.Repositories
{
    public class InstitutesRepository : IInstitutesRepository
    {
        private readonly GradeoDbContext _context;

        public InstitutesRepository(GradeoDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Institute>> Get()
        {
            return await _context.Institutes
                .Where(u => u.RemovedDate == null)
                .ToListAsync();
        }
    }
}
