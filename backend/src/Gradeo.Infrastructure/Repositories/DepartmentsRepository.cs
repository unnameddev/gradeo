﻿using Gradeo.Core.Abstractions;
using Gradeo.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gradeo.Infrastructure.Repositories
{
    public class DepartmentsRepository : IDepartmentsRepository
    {
        private readonly GradeoDbContext _context;

        public DepartmentsRepository(GradeoDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Department>> Get()
        {
            return await _context.Departments
                .Where(u => u.RemovedDate == null)
                .ToListAsync();
        }
    }
}
