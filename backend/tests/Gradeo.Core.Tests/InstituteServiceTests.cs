using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Gradeo.Core.Abstractions;
using Gradeo.Core.Entities;
using Gradeo.Core.Services;
using Moq;
using Xunit;

namespace Gradeo.Core.Tests
{
    public class InstituteServiceTests
    {
        [Fact]
        public async Task GetAll_ShouldReturn_AListOfDtos()
        {
            // Arrange-���� - �������������� ���������
            var repositoryMock = new Mock<IInstitutesRepository>();
            repositoryMock.Setup(m => m.Get()).ReturnsAsync(new List<Institute>
            {
                new Institute { Id = 1, Name = "Foo" },
                new Institute { Id = 2, Name = "Bar" },
            });

            var service = new InstitutesService(repositoryMock.Object);

            // Act-���� - ��������� ��, ��� ���������
            var result = await service.GetAll();

            // Assert-���� - ��������� ����������
            Assert.NotEmpty(result);
            Assert.Collection(result,
                item => Assert.Equal("Foo", item.Name),
                item => Assert.Equal("Bar", item.Name)
                );
        }
    }
}
